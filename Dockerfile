FROM node:latest as builder

COPY package.json package-lock.json ./
COPY . .

FROM nginx

RUN rm -rf /usr/share/nginx/html/*

#COPY nginx.conf /etc/nginx/nginx.conf
#COPY --from=builder /dist/cat-web/ /usr/share/nginx/html
COPY --from=builder /dist/cat-web/ /usr/share/nginx/html
